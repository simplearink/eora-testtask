from scipy.spatial import distance
from imutils.video import FileVideoStream
from imutils.video import VideoStream
from imutils import face_utils
import numpy as np
import argparse
import imutils
import time
import dlib
import cv2


# function that calculated EAR based on (x,y)-coordinates of 6 points which define an eye
def eye_aspect_ratio(eye_points):
    horisontal_dist = distance.euclidean(eye_points[0], eye_points[3])

    left_vertical = distance.euclidean(eye_points[1], eye_points[5])
    right_vertical = distance.euclidean(eye_points[2], eye_points[4])

    ear = (left_vertical + right_vertical) / (2 * horisontal_dist)

    return ear


ap = argparse.ArgumentParser()
ap.add_argument("-p", "--shape-predictor", required=True,
                help="path to facial landmark predictor")
ap.add_argument("-v", "--video", type=str, default="",
                help="path to input video file")
args = vars(ap.parse_args())

EYE_AR_THRESH = 0.31
EYE_AR_CONSEC_FRAMES = 3
ALERT_TIME_SEC = 2

FRAMES_COUNTER = 0
TOTAL_BLINKS = 0

print("[INFO] loading facial landmark predictor...")
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor(args["shape_predictor"])

(left_start, left_end) = face_utils.FACIAL_LANDMARKS_IDXS['left_eye']
(right_start, right_end) = face_utils.FACIAL_LANDMARKS_IDXS['right_eye']

# start the video stream thread
print("[INFO] starting video stream thread...")
fileStream = False if args["video"] == "" else True

if fileStream:
    vs = FileVideoStream(args["video"]).start()
    print("[INFO] File opened successfully")
else:
    vs = VideoStream(src=0).start()

time.sleep(1.0)

time_start = -100

while True:
    if fileStream and vs.Q.qsize() <= 1:  # if we deal with file, we need to see if there are frames left
        break
    # grab the frame from the threaded video file stream, resize
    # it, and convert it to grayscale channels)
    frame = vs.read()
    frame = imutils.resize(frame, width=450)
    # gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    # detect faces in the grayscale frame
    faces = detector(frame, 0)

    for face in faces:
        shape = predictor(frame, face)  # determine the facial landmarks for the face region
        shape = face_utils.shape_to_np(shape)  # coordinates to numpy-array

        left_eye = shape[left_start:left_end]
        right_eye = shape[right_start:right_end]

        left_ear = eye_aspect_ratio(left_eye)
        right_ear = eye_aspect_ratio(right_eye)

        avg_ear = (left_ear + right_ear) / 2

        # compute the convex hull for the left and right eye, then
        # visualize each of the eyes
        leftEyeHull = cv2.convexHull(left_eye)
        rightEyeHull = cv2.convexHull(right_eye)
        cv2.drawContours(frame, [leftEyeHull], -1, (0, 255, 0), 1)
        cv2.drawContours(frame, [rightEyeHull], -1, (0, 255, 0), 1)

        if avg_ear < EYE_AR_THRESH:
            if FRAMES_COUNTER == 0:
                time_start = time.time()
            else:
                if (time.time() - time_start >= 2):
                    cv2.putText(frame, "ALERT", (300, len(frame) - 50),
                                cv2.FONT_HERSHEY_SIMPLEX, 0.9, (0, 0, 255), 2)
            FRAMES_COUNTER += 1
        else:
            if FRAMES_COUNTER >= EYE_AR_CONSEC_FRAMES:
                TOTAL_BLINKS += 1

            FRAMES_COUNTER = 0

        # draw the total number of blinks on the frame along with
        # the computed eye aspect ratio for the frame
        cv2.putText(frame, "Blinks: {}".format(TOTAL_BLINKS), (10, 30),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)
        cv2.putText(frame, "EAR: {:.2f}".format(avg_ear), (300, 30),
                    cv2.FONT_HERSHEY_SIMPLEX, 0.7, (0, 0, 255), 2)

        # show the frame
        cv2.imshow("Frame", frame)
        key = cv2.waitKey(1) & 0xFF

        # if the `q` key was pressed, break from the loop
        if key == ord("q"):
            break

# do a bit of cleanup
cv2.destroyAllWindows()
vs.stop()
