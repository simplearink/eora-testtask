### Test task for EORA internship

**Arina Kuznetsova, 3rd year student** \
**E-mail**: a.kuznetsova@innopolis.ru \
**Telegram**: @simplearink

*Chosen topic*: Eye Blink Detection

#### Prerequisites
You need pre-installed **dlib**

#### Installation

First step is cloning this repo:

`git clone https://gitlab.com/simplearink/eora-testtask.git`

Then you should install all necessary libs:

`pip3 install -r requirements.txt`

#### Running the code

After everything is installed, you may start checking the solution

If you want to see how the detector works on video file, type the following in command line:

`python3 blinks_detector.py --shape-predictor ./shape_predictor_68_face_landmarks.dat
--video PATH_TO_YOUR_VIDEO`, e.g.

`python3 blinks_detector.py --shape-predictor
/home/arina/PycharmProjects/EORAtesting/shape_predictor_68_face_landmarks.dat
--video ./IMG_6929.MOV`

If you want to check results online (using your camera), just type this:

`python3 blinks_detector.py --shape-predictor
./shape_predictor_68_face_landmarks.dat`

#### Short description of the algorithm

The code uses **dlib** for face detection (eyes in particular). Each eye is defined as a set of 6 points.
The first one is the leftmost one. So, there are 2 points on horisontal line in the middle line of the eye,
and 2 points on upper part of eye and 2 points on lower part of eye.

These points coordinates are used to calculate Eye Aspect Ratio: Euclidian distance between 3 subsets of points is used.
We need to measure distance between points on horisontal line (p1 and p4), between left points from upper and lower parts
and between right points from upper and lower parts. EAR = sum of last 2 distances/ (2*first distance)

As the eyes closes, the distance between the second and third sets of points becomes small, so the ratio also becomes small.
If the computer notices that the ratio is smaller than threshold (0.3) for more than 3 consecutive frames,
than the eyes may be considered as closed and then we add 1 to amount of blinks.